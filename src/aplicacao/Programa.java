package aplicacao;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import entidades.Comentario;
import entidades.Post;

public class Programa {

	public static void main(String[] args) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Comentario c1 = new Comentario("Tenha uma �tima viagem(de acido)!");
		Comentario c2 = new Comentario("Que legal(sqn)!");
		Post p1 = new Post(sdf.parse("21/06/2018 13:05:44"), "Viajando mano", "Mais loco que o batma", 12);

		p1.addComentario(c1);
		p1.addComentario(c2);
		
		System.out.println(p1);
	}

}
